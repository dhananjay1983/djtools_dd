package org.djtools.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bidfta.cms.model.Banner;
import org.bidfta.cms.model.UserBanner;
import org.bidfta.cms.service.BannerService;
import org.bidfta.cms.service.UserBannerService;
import org.bidfta.config.core.MyUserPrincipal;
import org.bidfta.model.Auctions;
import org.bidfta.model.Bidders;
import org.bidfta.service.AuctionService;
import org.bidfta.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PropertySource("classpath:bidfta.properties")
public class LoginController {

	@Autowired
	LoginService loginService;
	
	@Autowired
	BannerService bannerService;

	@Autowired
	AuctionService auctionService;

	@Autowired
	UserBannerService userBannerService;

	@Value("${com.bidfta.auctions.default.loadlimit}")
	private int auctionsDefaultLoadLimit;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@RequestMapping(value = "/login")
	public ModelAndView viewLogin(ModelAndView model,@RequestParam(value= "source",required = false) String source,
			HttpServletRequest request) throws IOException {
		Bidders newBidder = new Bidders();
		request.getSession().setAttribute("email", null);
		request.getSession().setAttribute("idbidders", null);
		request.getSession().setAttribute("username", null);
		request.getSession().invalidate();
		
		if(source!=null){
			model.addObject("source", source);
		}
		model.addObject("bidders", newBidder);
		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "invalid", required = false) String invalid,
			@RequestParam(value= "source",required = false) String source,
			@ModelAttribute("msg") String msg, HttpServletRequest request) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
			model.setViewName("login");
		}
		if(msg!=null){
			model.addObject("msg", msg);
		}
		
		if(source!=null){
			model.addObject("source", source);
		}
		
		if (logout != null) {
			request.getSession().setAttribute("email", null);
			request.getSession().setAttribute("idbidders", null);
			request.getSession().setAttribute("username", null);
			request.getSession().invalidate();
			model.addObject("msg", "You've been logged out successfully.");
			model.setViewName("login");
		}
	
		if (invalid != null) {
			request.getSession().setAttribute("email", null);
			request.getSession().setAttribute("idbidders", null);
			request.getSession().setAttribute("username", null);
			request.getSession().invalidate();
			model.addObject("msg", "You've been logged out successfully.");
			model.setViewName("redirect:/login?logout=true");
		}

		model.setViewName("login");
		return model;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout=true";
	}

	// @RequestMapping(value = "/user/bidderHome", method = RequestMethod.GET)
	// public ModelAndView loginBidder(@ModelAttribute Bidders bidder,
	// HttpServletRequest request, Model model) {

	// Set<Validation> preLoginValidations =
	// loginService.preLoginValidation(bidder);
	// for (Validation validation : preLoginValidations) {
	// model.addAttribute(validation.getFormField(),
	// validation.getValidationMessage());
	// return new ModelAndView(validation.getModelObject());
	// }
	// Bidders retrunBidder = loginService.checkLogin(bidder);
	//
	// if (null != retrunBidder) {
	// Set<Validation> postLoginValidations =
	// loginService.postLoginValidation(retrunBidder, bidder.getPassword());
	// if (postLoginValidations.size() > 0) {
	// for (Validation validation : postLoginValidations) {
	// model.addAttribute(validation.getFormField(),
	// validation.getValidationMessage());
	// return new ModelAndView(validation.getModelObject());
	// }
	// } else {
	// Must be called from request filtered by Spring Security, otherwise
	// SecurityContextHolder is not updated
	// UsernamePasswordAuthenticationToken token = new
	// UsernamePasswordAuthenticationToken(bidder.getEmail(),
	// bidder.getPassword());
	// token.setDetails(new WebAuthenticationDetails(request));
	// MyUserPrincipal m = (MyUserPrincipal)
	// SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	// String username = m.getUsername();
	// ModelAndView modelView = new ModelAndView("home");
	// request.getSession().setAttribute("username", username);
	// request.getSession().setAttribute("email", username);
	// request.getSession().setAttribute("idbidders",
	// m.getBidders().getIdbidders());
	//
	// List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(null,
	// null, null, null,
	// auctionsDefaultLoadLimit);
	// modelView.addObject("auctions", auctions);
	// List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
	// modelView.addObject("featuredAuctions", featuredAuctions);
	//
	// List<Banner> banners = bannerService.getAllBanners();
	// Auctions auction = new Auctions();
	// auction.setAuctionsSize(auctionsDefaultLoadLimit);
	// modelView.addObject("auctionsFilter", auction);
	// modelView.addObject("auctionsSort", auction);
	// modelView.addObject("auctionsLoad", auction);
	// modelView.addObject("auctionsSearch", auction);
	// modelView.addObject("banners", banners);
	// return modelView;
	// }
	// } else {
	// model.addAttribute("error", "Invalid Email Address/Password.");
	// return new ModelAndView("redirect:/login");
	// }
	// return new ModelAndView("redirect:/login");
	// }

	// This method is called when user is not signed in.
	@RequestMapping(value = "/bidderHome", method = RequestMethod.GET)
	public ModelAndView loginBidderHome(@ModelAttribute Bidders bidder, HttpServletRequest request, Model model) {
		String username = null;
		try {
			MyUserPrincipal m = (MyUserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

			if (m.getBidders().getEmailverificationstatus() == 0) {
				ModelAndView modelView = new ModelAndView("login");
				modelView.addObject("error", "Please verify email address.");
				return modelView;
			}
			if (m.getBidders().getSmsverificationstatus() == 0) {
				ModelAndView modelView = new ModelAndView("login");
				modelView.addObject("error", "Please verify SMS Number.");
				return modelView;
			}

			username = m.getUsername();
			request.getSession().setAttribute("username", username);
			request.getSession().setAttribute("email", username);
			request.getSession().setAttribute("idbidders", m.getBidders().getIdbidders());
		} catch (Exception e) {
			// if the user details are not available then continue.
		}

		ModelAndView modelView = new ModelAndView("home");
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(null, null, null, null,
				auctionsDefaultLoadLimit);
		modelView.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		modelView.addObject("featuredAuctions", featuredAuctions);

		if (username == null) {
			List<Banner> banners = bannerService.getAllBanners();
			modelView.addObject("banners", banners);
		} else {
			UserBanner activeUserBanner = userBannerService.getActiveUserBanner();
			modelView.addObject("activeUserBanner", activeUserBanner);			
		}
		
		Auctions auction = new Auctions();
		auction.setAuctionsSize(auctionsDefaultLoadLimit);
		modelView.addObject("auctionsFilter", auction);
		modelView.addObject("auctionsSort", auction);
		modelView.addObject("auctionsLoad", auction);
		modelView.addObject("auctionsSearch", auction);
		
		return modelView;
	}

	@ModelAttribute("auctionAllCategories")
	public List<String> populateAuctionAllCategories() {
		ArrayList<String> auctionAllCategories = new ArrayList<>();
		auctionAllCategories.add("Categories");
		auctionAllCategories.addAll(auctionService.getAllCategories());
		return auctionAllCategories;
	}

	@ModelAttribute("auctionAllAreas")
	public List<String> populateAuctionAllAreas() {
		ArrayList<String> auctionAllAreas = new ArrayList<>();
		auctionAllAreas.add("Area");
		auctionAllAreas.addAll(auctionService.getAllAreas());
		return auctionAllAreas;
	}

	@ModelAttribute("auctionSortBy")
	public List<String> populateAuctionSortBy() {
		ArrayList<String> auctionSortBy = new ArrayList<>();
		auctionSortBy.add("Sort By");
		auctionSortBy.add("Ends");
		auctionSortBy.add("Starts");
		auctionSortBy.add("Title");
		return auctionSortBy;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {

		ModelAndView model = new ModelAndView("error-generic");
		model.addObject("exception", ex);
		return model;

	}
}
