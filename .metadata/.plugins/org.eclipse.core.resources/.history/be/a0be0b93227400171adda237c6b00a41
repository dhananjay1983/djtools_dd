package org.bidfta.controller;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.bidfta.model.ContactUs;
import org.bidfta.model.Validation;
import org.bidfta.service.ContactusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ContactusController {

	@Autowired
	ContactusService contactUsService;

	@Value("${com.bidfta.captcha.sitekey}")
	private String captchaSiteKey;

	@Value("${com.bidfta.captcha.secretkey}")
	private String captchaSecretKey;

	@RequestMapping(value = "/contactus")
	public ModelAndView getDashboard(ModelAndView model, HttpServletRequest request) throws IOException {
		model.addObject("contactUsdata", new ContactUs());
		model.setViewName("contactus");
		return model;
	}

	@RequestMapping(value = "/saveContactUs", method = RequestMethod.POST)
	public ModelAndView saveContactUs(@Valid @ModelAttribute("contactUsdata") @Validated ContactUs contactus,
			BindingResult result, HttpServletRequest request, ModelAndView model) {
		// get reCAPTCHA request param
		String gRecaptchaResponse = request.getParameter("g-recaptcha-response");
		Set<Validation> validations = contactUsService.validateContact(gRecaptchaResponse, captchaSecretKey);
		for (Validation validation : validations) {
			result.rejectValue(validation.getFormField(), validation.getModelObject(),
					validation.getValidationMessage());
		}
		if (result.hasErrors()) {
			model.addObject("contactUsdata", contactus);
		} else {
			contactUsService.saveContactUs(contactus);
			model.addObject("contactUsdata", new ContactUs());
		}

		model.setViewName("contactus");
		return model;
	}

}
