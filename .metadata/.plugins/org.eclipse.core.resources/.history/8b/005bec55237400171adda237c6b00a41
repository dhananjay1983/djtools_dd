package org.djtools.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.bidfta.cms.model.Banner;
import org.bidfta.cms.service.BannerService;
import org.bidfta.cms.service.SystemConfigService;
import org.bidfta.model.Auctions;
import org.bidfta.service.AuctionService;
import org.bidfta.service.LocationService;
import org.bidfta.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.StringUtils;

/**
 * Description.
 * 
 * @author ronak.patel <roncpat@gmail.com>
 *
 */
@Controller
@PropertySource("classpath:bidfta.properties")
public class AuctionController {

	private static final Logger logger = Logger.getLogger(DataDictionaryController.class);

	@Autowired
	AuctionService auctionService;

	@Autowired
	LocationService locationService;

	@Autowired
	BannerService bannerService;

	@Autowired
	SystemConfigService systemConfigService;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@RequestMapping(value = "/")
	public ModelAndView viewHome(ModelAndView model) throws IOException {
		logger.info("Home page is loading");
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(null, null, null, null,
				Constants.LOADLIMIT);
		model.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		model.addObject("featuredAuctions", featuredAuctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(Constants.LOADLIMIT);
		model.addObject("auctionsFilter", auction);
		model.addObject("auctionsSort", auction);
		model.addObject("auctionsLoad", auction);
		model.addObject("auctionsSearch", auction);

		addBanner(model);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/home")
	public ModelAndView viewHomeFromHeader(ModelAndView model) throws IOException {
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(null, null, null, null,
				Constants.LOADLIMIT);
		model.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		model.addObject("featuredAuctions", featuredAuctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(Constants.LOADLIMIT);
		model.addObject("auctionsFilter", auction);
		model.addObject("auctionsSort", auction);
		model.addObject("auctionsLoad", auction);
		model.addObject("auctionsSearch", auction);
		addBanner(model);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/filterAuctions", method = RequestMethod.POST)
	public ModelAndView filterAuctions(@ModelAttribute("auctionsFilter") Auctions formAuctions, ModelAndView model) {

		Set<Integer> idLocations = null;
		if (!StringUtils.isNullOrEmpty(formAuctions.getArea()) && !"Area".equals(formAuctions.getArea())) {
			idLocations = locationService.getLocationByCityState(formAuctions.getArea());
		}
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(idLocations, formAuctions.getCategory(),
				formAuctions.getSortBy(), formAuctions.getSearchKeywords(), formAuctions.getAuctionsSize());
		model.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		model.addObject("featuredAuctions", featuredAuctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(formAuctions.getAuctionsSize());
		auction.setCategory(formAuctions.getCategory());
		auction.setArea(formAuctions.getArea());
		auction.setSortBy(formAuctions.getSortBy());
		auction.setSearchKeywords(formAuctions.getSearchKeywords());
		auction.setCurrentView(formAuctions.getCurrentView());

		model.addObject("auctionsFilter", auction);
		model.addObject("auctionsSort", auction);
		model.addObject("auctionsLoad", auction);
		model.addObject("auctionsSearch", auction);

		addBanner(model);

		model.setViewName("home");
		return model;
	}

	private void addBanner(ModelAndView model) {
		List<Banner> banners = bannerService.getActiveBanners();
		String bannerTimmer = systemConfigService.getSystemConfigValueByKey(Constants.SystemConfig.BANNER_TIMMER);
		model.addObject("banners", banners);
		model.addObject("bannertimmer", bannerTimmer);
	}

	@RequestMapping(value = "/sortAuctions", method = RequestMethod.POST)
	public ModelAndView sortAuctions(@ModelAttribute("auctionsSort") Auctions formAuctions, ModelAndView model) {

		Set<Integer> idLocations = null;
		if (!StringUtils.isNullOrEmpty(formAuctions.getArea()) && !"Area".equals(formAuctions.getArea())) {
			idLocations = locationService.getLocationByCityState(formAuctions.getArea());
		}
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(idLocations, formAuctions.getCategory(),
				formAuctions.getSortBy(), formAuctions.getSearchKeywords(), formAuctions.getAuctionsSize());
		model.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		model.addObject("featuredAuctions", featuredAuctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(formAuctions.getAuctionsSize());
		auction.setCategory(formAuctions.getCategory());
		auction.setArea(formAuctions.getArea());
		auction.setSortBy(formAuctions.getSortBy());
		auction.setSearchKeywords(formAuctions.getSearchKeywords());
		auction.setCurrentView(formAuctions.getCurrentView());

		model.addObject("auctionsFilter", auction);
		model.addObject("auctionsSort", auction);
		model.addObject("auctionsLoad", auction);
		model.addObject("auctionsSearch", auction);
		addBanner(model);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/loadMoreAuctions", method = RequestMethod.POST)
	public ModelAndView loadMoreAuctions(@ModelAttribute("auctionsLoad") Auctions formAuctions, ModelAndView model) {

		Set<Integer> idLocations = null;
		if (!StringUtils.isNullOrEmpty(formAuctions.getArea()) && !"Area".equals(formAuctions.getArea())) {
			idLocations = locationService.getLocationByCityState(formAuctions.getArea());
		}
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(idLocations, formAuctions.getCategory(),
				formAuctions.getSortBy(), formAuctions.getSearchKeywords(),
				(Constants.LOADLIMIT + formAuctions.getAuctionsSize()));
		model.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		model.addObject("featuredAuctions", featuredAuctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(Constants.LOADLIMIT + formAuctions.getAuctionsSize());
		auction.setCategory(formAuctions.getCategory());
		auction.setArea(formAuctions.getArea());
		auction.setSortBy(formAuctions.getSortBy());
		auction.setSearchKeywords(formAuctions.getSearchKeywords());
		auction.setCurrentView(formAuctions.getCurrentView());

		model.addObject("auctionsFilter", auction);
		model.addObject("auctionsSort", auction);
		model.addObject("auctionsLoad", auction);
		model.addObject("auctionsSearch", auction);
		addBanner(model);
		model.setViewName("home");
		return model;
	}
	
	@RequestMapping(value = "/getAuctions", method = RequestMethod.POST, produces = "application/json") ///user
	@ResponseBody
	public Map<String, Object> getAuctions(@RequestParam("pageId") Integer pageId, @RequestParam("auctionsSize") Integer auctionsSize, 
			@RequestParam("category") String category, 	@RequestParam("area") String area, @RequestParam("sortBy") String sortBy, 
			@RequestParam("searchKeywords") String searchKeywords, @RequestParam("currentView") String currentView) {

		Map<String, Object> dataMap = new HashMap<String, Object>();
		Set<Integer> idLocations = null;
		if (!StringUtils.isNullOrEmpty(area) && !"Area".equals(area)) {
			idLocations = locationService.getLocationByCityState(area);
		}
		List<Auctions> auctions = auctionService.getAuctionsByPage(idLocations, category,
				sortBy, searchKeywords,
				(Constants.LOADLIMIT + auctionsSize), pageId);
		dataMap.put("data", auctions);
		if (auctions.size() > 0) {
			dataMap.put("success", true);
		} else {
			dataMap.put("success", false);
		}
		
		dataMap.put("loadedauctionsSize", auctions.size() + auctionsSize);
		dataMap.put("auctionsSize", Constants.LOADLIMIT + auctionsSize);
		dataMap.put("category", category);
		dataMap.put("area", area);
		dataMap.put("sortBy", sortBy);
		dataMap.put("searchKeywords", searchKeywords);
		dataMap.put("currentView", currentView);

		return dataMap;
	}
	
	@RequestMapping(value = "/searchAuctions", method = RequestMethod.POST)
	public ModelAndView searchAuctions(@ModelAttribute("auctionsSearch") Auctions formAuctions, ModelAndView model) {

		Set<Integer> idLocations = null;
		if (!StringUtils.isNullOrEmpty(formAuctions.getArea()) && !"Area".equals(formAuctions.getArea())) {
			idLocations = locationService.getLocationByCityState(formAuctions.getArea());
		}
		List<Auctions> auctions = auctionService.getAuctionsByAreaNCategory(idLocations, formAuctions.getCategory(),
				formAuctions.getSortBy(), formAuctions.getSearchKeywords(), formAuctions.getAuctionsSize());
		model.addObject("auctions", auctions);
		List<Auctions> featuredAuctions = auctionService.getFeaturedAuctions();
		model.addObject("featuredAuctions", featuredAuctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(formAuctions.getAuctionsSize());
		auction.setCategory(formAuctions.getCategory());
		auction.setArea(formAuctions.getArea());
		auction.setSortBy(formAuctions.getSortBy());
		auction.setSearchKeywords(formAuctions.getSearchKeywords());
		auction.setCurrentView(formAuctions.getCurrentView());

		model.addObject("auctionsFilter", auction);
		model.addObject("auctionsSort", auction);
		model.addObject("auctionsLoad", auction);
		model.addObject("auctionsSearch", auction);
		addBanner(model);
		model.setViewName("home");
		return model;
	}

	@RequestMapping(value = "/auctionDetails")
	public ModelAndView getAuctionDetails(ModelAndView model, @RequestParam("idauctions") Integer idauctions)
			throws IOException {
		Auctions auction = auctionService.getAuctionById(idauctions);
		model.addObject("auction", auction);
		model.setViewName("auctionDetails");
		return model;
	}
	
	@RequestMapping(value = "/pastAuctions")
	public ModelAndView pastAuctions(ModelAndView model) throws IOException {
		List<Auctions> auctions = auctionService.getPastAuctions(null, Constants.LOADLIMIT);
		model.addObject("auctions", auctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(Constants.LOADLIMIT);

		model.addObject("pastAuctionsLoad", auction);
		model.addObject("pastAuctionsSearch", auction);
		model.setViewName("past-auctions");
		return model;
	}
	
	@RequestMapping(value = "/getPastAuctions", method = RequestMethod.POST, produces = "application/json") ///user
	@ResponseBody
	public Map<String, Object> getPastAuctions(@RequestParam("pageId") Integer pageId, @RequestParam("auctionsSize") Integer auctionsSize, 
			@RequestParam("searchKeywords") String searchKeywords) {

		Map<String, Object> dataMap = new HashMap<String, Object>();
		
		List<Auctions> auctions = auctionService.getPastAuctionsByPage(searchKeywords, (Constants.LOADLIMIT + auctionsSize), pageId);
		dataMap.put("data", auctions);
		if (auctions.size() > 0) {
			dataMap.put("success", true);
		} else {
			dataMap.put("success", false);
		}
		
		dataMap.put("loadedauctionsSize", auctions.size() + auctionsSize);
		dataMap.put("auctionsSize", Constants.LOADLIMIT + auctionsSize);
		dataMap.put("searchKeywords", searchKeywords);

		return dataMap;
	}

	@RequestMapping(value = "/searchPastAuctions", method = RequestMethod.POST)
	public ModelAndView searchPastAuctions(@ModelAttribute("pastAuctionsSearch") Auctions formAuctions,
			ModelAndView model) {
		List<Auctions> auctions = auctionService.getPastAuctions(formAuctions.getSearchKeywords(),
				formAuctions.getAuctionsSize());
		model.addObject("auctions", auctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(formAuctions.getAuctionsSize());
		auction.setSearchKeywords(formAuctions.getSearchKeywords());

		model.addObject("pastAuctionsLoad", auction);
		model.addObject("pastAuctionsSearch", auction);
		model.setViewName("past-auctions");
		return model;
	}

	@RequestMapping(value = "/loadMorePastAuctions", method = RequestMethod.POST)
	public ModelAndView loadMorePastAuctions(@ModelAttribute("pastAuctionsLoad") Auctions formAuctions,
			ModelAndView model) {
		List<Auctions> auctions = auctionService.getPastAuctions(formAuctions.getSearchKeywords(),
				(Constants.LOADLIMIT + formAuctions.getAuctionsSize()));
		model.addObject("auctions", auctions);

		Auctions auction = new Auctions();
		auction.setAuctionsSize(Constants.LOADLIMIT + formAuctions.getAuctionsSize());
		auction.setSearchKeywords(formAuctions.getSearchKeywords());

		model.addObject("pastAuctionsLoad", auction);
		model.addObject("pastAuctionsSearch", auction);
		model.setViewName("past-auctions");
		return model;
	}

	@ModelAttribute("auctionAllCategories")
	public List<String> populateAuctionAllCategories() {
		ArrayList<String> auctionAllCategories = new ArrayList<>();
		auctionAllCategories.add("Categories");
		auctionAllCategories.addAll(auctionService.getAllCategories());
		return auctionAllCategories;
	}

	@ModelAttribute("auctionAllAreas")
	public List<String> populateAuctionAllAreas() {
		ArrayList<String> auctionAllAreas = new ArrayList<>();
		auctionAllAreas.add("Area");
		auctionAllAreas.addAll(auctionService.getAllAreas());
		return auctionAllAreas;
	}

	@ModelAttribute("auctionSortBy")
	public List<String> populateAuctionSortBy() {
		ArrayList<String> auctionSortBy = new ArrayList<>();
		auctionSortBy.add("Sort By");
		auctionSortBy.add("Ends");
		auctionSortBy.add("Starts");
		auctionSortBy.add("Title");
		return auctionSortBy;
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {

		ModelAndView model = new ModelAndView("error-generic");
		model.addObject("exception", ex);
		return model;

	}
}
